package com.example.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class ApplicationController {
	@Autowired
	UserService userService;

	// inject via application.properties
	@Value("${welcome.message}")
	private String message = "Hello World";

	@RequestMapping(value={"/","/home"})
	public ModelAndView home(){  
		return new ModelAndView("home");  
	} 

	@RequestMapping("/login")  
	public ModelAndView index(){  
		return new ModelAndView("login");  
	}  

	@RequestMapping("/signUp")  
	public ModelAndView signUpPage(){  
		return new ModelAndView("sign-up");  
	}  

	@RequestMapping(value="/all-users")  
	public List<UserRecord> getAllUser(){  
		return userService.getAllUsers();  
	}  

	@RequestMapping(value="/view-all-users")  
	public ModelAndView viewAllUsers(){  
		ModelAndView modelAndView = new ModelAndView();  
		modelAndView.setViewName("view-users");      
		modelAndView.addObject("userList", userService.getAllUsers());    
		return modelAndView;  
	}  

	@RequestMapping(value="/save", method=RequestMethod.POST)  
	public ModelAndView save(@ModelAttribute UserRecord user){  
		ModelAndView modelAndView = new ModelAndView();  
		modelAndView.setViewName("user-successfull-login");      
		modelAndView.addObject("user", user);    
		return modelAndView;  
	}  

	@RequestMapping(value="/addUser", method=RequestMethod.POST)  
	public ModelAndView addUser(@ModelAttribute UserRecord userRecord){
		userService.addUser(userRecord);  
		ModelAndView modelAndView = new ModelAndView();  
		modelAndView.setViewName("home");      
		return modelAndView;  
	}  

	//@DeleteMapping(value="/deleteUser/{id}")
	@RequestMapping(value = "deleteUser", method = RequestMethod.GET)
	public ModelAndView deleteUser(@RequestParam("id") Integer userId){
		UserRecord userRecord = userService.findOne(userId);
		userService.deleteUser(userRecord);
		ModelAndView modelAndView = new ModelAndView();  
		modelAndView.setViewName("view-users");      
		modelAndView.addObject("userList", userService.getAllUsers());    
		return modelAndView; 
	} 
	
	@RequestMapping(value = "deleteUserConfirm", method = RequestMethod.GET)
	public ModelAndView deleteUserConfirm(@RequestParam("id") Integer userId){
		UserRecord userRecord = userService.findOne(userId);
		ModelAndView modelAndView = new ModelAndView();  
		modelAndView.setViewName("delete-user-confirm");      
		modelAndView.addObject("user", userRecord);    
		return modelAndView; 
	} 

	//@PutMapping("/modifyUserSubmit")
	@RequestMapping(value = "modifyUserSubmit", method = RequestMethod.POST)
	public ModelAndView modifyUserSubmit(@RequestParam("id") Integer userId, @ModelAttribute UserRecord userRecord) {
		//userService.addUser(userRecord); //Both works fine for modify as well
		userService.save(userRecord);
		ModelAndView modelAndView = new ModelAndView();  
		modelAndView.setViewName("view-users");      
		modelAndView.addObject("userList", userService.getAllUsers());    
		return modelAndView; 
	}

	//@GetMapping("modifyUserDetails") ----- Either use it directly for GET
	@RequestMapping(value = "modifyUserDetails", method = RequestMethod.GET)
	public ModelAndView modifyUserDetails(@RequestParam("id") Integer userId){
		UserRecord userRecord = userService.findOne(userId);
		ModelAndView modelAndView = new ModelAndView();  
		modelAndView.setViewName("modify-user-details");      
		modelAndView.addObject("user", userRecord);    
		return modelAndView; 
	} 

	@RequestMapping(value="/go-back")  
	public ModelAndView goBack(@ModelAttribute UserRecord user){  
		ModelAndView modelAndView = new ModelAndView();  
		modelAndView.setViewName("user-successfull-login");      
		modelAndView.addObject("user", user);    
		return modelAndView;  
	}  

	@RequestMapping(value="/logout", method=RequestMethod.GET)
	public ModelAndView logout() {
		return new ModelAndView("index");
	}

	@RequestMapping("/welcome")
	public String welcome(Map<String, Object> model) {
		model.put("message", this.message);
		return "welcome";
	}
}