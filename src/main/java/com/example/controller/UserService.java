package com.example.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
	@Autowired 
	UserRepository userRepository;

	public List<UserRecord> getAllUsers(){  
		List<UserRecord> userRecords = new ArrayList<>();  
		userRepository.findAll().forEach(userRecords::add);  
		return userRecords;  
	}  

	public void addUser(UserRecord userRecord){  
		userRepository.save(userRecord);  
	}  
	
	public void deleteUser(UserRecord userRecord) {
		userRepository.delete(userRecord);
	}
	
	public UserRecord findOne(Integer userId) {
		return userRepository.findOne(userId);
	}
	
	public UserRecord save(UserRecord userRecord) {
		return userRepository.save(userRecord);
	}
	
	public void deleteById(Integer id) {
		userRepository.deleteById(id);
	}

}
