package com.example.controller;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity  
public class UserRecord {  
	@Id  
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int id; 
	@Column(name = "name")
	public String name;  
	@Column(name = "email")
	public String email;  

	public UserRecord(){}

	@GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {  
		return id;  
	}  
	public void setId(int id) {  
		this.id = id;  
	}  
	public String getName() {  
		return name;  
	}  
	public void setName(String name) {  
		this.name = name;  
	}  
	public String getEmail() {  
		return email;  
	}  
	public void setEmail(String email) {  
		this.email = email;  
	}  
}  