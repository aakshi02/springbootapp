package com.example.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootWebThymeleafApplication {
	private static final Logger LOGGER = LoggerFactory.getLogger(SpringBootWebThymeleafApplication.class);
			//getLogger(SpringBootWebThymeleafApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(SpringBootWebThymeleafApplication.class, args);
		LOGGER.info("Application has been initialised");
	}
}
